﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableObstacleButTrash : MonoBehaviour
{



    //Este script quizas lo use en un futuro pero con cambios obviamente.
    //Iba a usar este script mio pero debido a la cantidad INMENSA DE BUGS y la cantidad de tiempo que intente lidear con ellos lo dejare como borrador. Y si te interesa pues puedes mirar este script
    //Este script teoricamente es de un gameobject que uso para moverlo de izquierda a derecha y que cuando tu estes encima, te puedes quedar alli junto el momentum segun unos calculos que he hecho a mano.
    //Funciona pero solo si el personaje no tiene sistema de giro (osea si tiene freezeado los 3 ejes de rotacion), cuando gira como que tiene varios bugs. Uno es que no me deja avanzar el personaje si la rotacion y no esta a 0. El personaje se escala a lo random cuando encima el setparent lo tnego a true (tambien intente en false ajustando cosas y lo mismo)...
    //Resumen: valgo para trabajar en bugisoft. El que uso es el MovableObstacle sin el trash. Y en vez de quedarte encima alli, pues al tocar saltas automaticamente.

    //Estos booleans y floats son unas cosas de un calculo que los necesito publicos.
    bool isMoved = true;
    bool obstacleNegative;
    //True si es negativo su x
    float positionObstacleX;
    bool playerNegative;
    float positionPlayerX;





    int count= 0;
    private bool notEffective;
    //Serializablefield con rango lo que hace es poner en el editor un ajustador (no se como llamarlo) o algo asi
    [SerializeField (), Range(1, 20)]
    int distance;
    //Es un boolean que lo usaremos para indicar a donde movera. False empezara a la derecha, true a la izquierda 
    public bool direction= false;
    void Start()
    {
        //GameManager.UnHook += GameManager_UnHook;
        StartCoroutine(moving());
    }

    private void GameManager_UnHook(bool b)
    {
        //Se que es un poco guarro, pero es lo unico que se me ha ocurrido
        notEffective = b;
    }

    private IEnumerator moving()
    {
        //Entra a la corrutina, la primera vez espera 1 segundo, cambia el booleano y vuelve a entrar a su corrutina y asi continuadamente.
        //Teoricamente no tendria que petar el codigo ya que inicializo la corrutina con un start.
        yield return new WaitForSeconds(1f);
        direction = !direction;
        StartCoroutine(moving());
    }

    // Update is called once per frame
    void Update()
    {
        //Aqui lo que hago es que el player tenga la misma posicion x que la plataforma, para que se quede en pie (siempre que el player sea hijo de la plataforma, que se le asigna en OnCollisionEnter)
        //if(Input.GetA)
        //Debug.Log("Obstacle x" + this.transform.position.x);
        mathgoesbrr();
        
        //Debug.Log(this.GetComponent<Rigidbody>().velocity.x);
        //Cada segundo el boolean cambia, y se movera a la izquierda, luego a la derecha, y asi continuadamente.
        if (direction) this.GetComponent<Rigidbody>().velocity = new Vector3(-distance, this.GetComponent<Rigidbody>().velocity.y, this.GetComponent<Rigidbody>().velocity.z);
        else this.GetComponent<Rigidbody>().velocity = new Vector3(distance, this.GetComponent<Rigidbody>().velocity.y, this.GetComponent<Rigidbody>().velocity.z);
        
    }














    private void mathgoesbrr()
    {
        //childcount contara cuantos hijos tiene. En este caso si tiene almenos un hijo, entra
        if (this.GetComponent<Transform>().childCount > 0)
        {
            if(Input.GetAxis("Vertical")!= 0)
            {
                //De momento para prevenir bugs, pondre una velocidad predeterminada. Como el rigidbody del jugador equivale al de la plataforma, necesitare moverlo por alguna extraña razon.
                //Es un bug raro porque transform left me deja moverlo.
                //this.GetComponent<Transform>().GetChild(0).transform.eulerAngles = new Vector3(0, 0, 0);
                
            }
            //this.GetComponent<Transform>().GetChild(0).transform.localScale = new Vector3(1/this.transform.localScale.x, 1/this.transform.localScale.y, 1/this.transform.localScale.z);

            if (Input.GetAxis("Horizontal") == 0 && isMoved)
            {

                isMoved = false;

                //Padre (Obstacle)



                if (this.transform.position.x < 0)
                {
                    positionObstacleX = -(this.transform.position.x);
                    obstacleNegative = true;
                    //Debug.Log("real position " + this.transform.position.x + " Position without minus " + positionObstacleX);
                }
                else
                {
                    positionObstacleX = this.transform.position.x;
                    obstacleNegative = false;
                }


                //Hijo (Player)



                if (this.transform.position.x < 0)
                {
                    positionPlayerX = -(this.GetComponent<Transform>().GetChild(0).transform.position.x);
                    playerNegative = true;
                }
                else
                {
                    positionPlayerX = this.GetComponent<Transform>().GetChild(0).transform.position.x;
                    playerNegative = false;
                }
            }
            else if(Input.GetAxis("Horizontal") != 0)
            {
                
                isMoved = true;
                
            }
            //Debug.Log("Boolean esta a " + isMoved);
            //Debug.Log(positionPlayerX);
            
            if (!isMoved)
            {
                //Si es positivo las 2 x, el resultado a la derecha, si es negativo, a la izquierda.
                //Ojo al dato, date cuenta que si son negativos, los floats de positionobstaclex y positionplayerx los pasan en positivo. Por lo que no existen valores negativos y tendras que pasarles a negativo cuando sea necesario.
                if (obstacleNegative)
                {
                    if (playerNegative)
                    {
                        float result = positionObstacleX - positionPlayerX;

                        this.GetComponent<Transform>().GetChild(0).transform.position = new Vector3(
                            this.transform.position.x + result,
                            this.GetComponent<Transform>().GetChild(0).transform.position.y,
                            this.GetComponent<Transform>().GetChild(0).transform.position.z);


                    }
                    else if (!playerNegative)
                    {
                        //Puede que sume o reste la posicion, depende de los valores.
                        float result = -(positionObstacleX) + positionPlayerX;
                        this.GetComponent<Transform>().GetChild(0).transform.position = new Vector3(
                            this.transform.position.x - result,
                            this.GetComponent<Transform>().GetChild(0).transform.position.y,
                            this.GetComponent<Transform>().GetChild(0).transform.position.z);
                    }
                }
                else if (!obstacleNegative)
                {
                    if (playerNegative)
                    {
                        //Puede que sume o reste, depende de los valores.
                        float result = positionObstacleX - positionPlayerX;
                        this.GetComponent<Transform>().GetChild(0).transform.position = new Vector3(
                            this.transform.position.x + result,
                            this.GetComponent<Transform>().GetChild(0).transform.position.y,
                            this.GetComponent<Transform>().GetChild(0).transform.position.z);
                    }
                    else if (!playerNegative)
                    {
                        float result = positionObstacleX - positionPlayerX;
                        this.GetComponent<Transform>().GetChild(0).transform.position = new Vector3(
                            this.transform.position.x - result,
                            this.GetComponent<Transform>().GetChild(0).transform.position.y,
                            this.GetComponent<Transform>().GetChild(0).transform.position.z);
                    }
                }
            }
            
        }
        
        
        
    }


















    private void OnCollisionEnter(Collision collision)
    {
        //&& !notEffective
        if (collision.gameObject.name == "player")
        {
            Debug.LogWarning("Player detected, changing gravity");
            collision.rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            collision.transform.SetParent(this.transform, true);
            //Tienes que indicarle que el collision es un gameobject si quieres editar su rigidbody.
            //collision.gameObject.GetComponent<Rigidbody>().velocity = -transform.up;
            //collision.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(collision.gameObject.GetComponent<Rigidbody>().velocity.x + this.GetComponent<Rigidbody>().velocity.x*2, collision.gameObject.GetComponent<Rigidbody>().velocity.y, collision.gameObject.GetComponent<Rigidbody>().velocity.z);
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.name == "player")
        {
            Debug.LogWarning("Player is not in "+this.gameObject.name+", changing gravity to normal");
            collision.gameObject.GetComponent<Transform>().parent = null;
            //Se reinicia el isMoved por si llega a estar en false, porque si esta en false, cuando vuelvas a entrar en la plataforma, te pondra la posicion anterior ya que los floats se cambian solo al haberte movido (estando en true si te has movido).
            isMoved = true;
        }
    }
}
