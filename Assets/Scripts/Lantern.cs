﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lantern : MonoBehaviour
{
    Light lnt;
    // Start is called before the first frame update
    void Start()
    {
        lnt = this.GetComponent<Transform>().GetComponentInChildren<Light>();
        Debug.Log(lnt);
        //Lo desactivo al principio, porque no lo necesitaremos
        lnt.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Puedes cambiar la opcion de este boolean desde el menu. Predeterminado esta desactivado.
        //Debug.Log(DifficultyMaker.lanternMove);
        if (DifficultyMaker.lanternMove)
        {
            this.GetComponent<Transform>().transform.Rotate(Input.GetAxis("RightStick Vertical"), 0, 0);
            limitView();
        }
        //Debug.Log(Input.GetAxis("RightStick Vertical"));
        //Es un Input que he creado yo para mandos.
        
        //Button8 es LS_B o L3
        if (Input.GetKeyDown("f") || Input.GetKeyDown(KeyCode.Joystick1Button8))
        {
            Debug.Log("Pressed Key");
            lnt.enabled = !lnt.enabled;
        }
    }

    //Esto no funciona mucho, asi que tiro la toalla con esto.
    private void limitView()
    {
        this.transform.Rotate(Input.GetAxis("Mouse Y"), 0, 0);
        //Resulta que eulerangles es muy inteligente y lo que hace es que los valores negativos los cuenta positivos. Me explico: si en la rotacion x esta -1, el eulerangles lo leera como 359
        //Asi que para no liarlo, pues he tenido que mirar como volverlo negativo
        float angle = this.transform.eulerAngles.x;
        //angle = (angle > 180) ? angle - 360 : angle;
        if(angle > 180)
        {
            angle = angle - 360;
        }
        GameObject player = GameObject.Find("player");
        //Debug.Log(angle);
        //30
        if (angle <= -31)
        {
            this.transform.eulerAngles = new Vector3(-30, this.transform.eulerAngles.y, 0);
        }
        else if(angle >= 31)
        {
            this.transform.eulerAngles = new Vector3(30, this.transform.eulerAngles.y, 0);
        }
    }
}
