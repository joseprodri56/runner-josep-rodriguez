﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionsPlayer : MonoBehaviour
{
    public Material dissapear;
    //Si llega a true, significa que has pasado el nivel.
    public static bool levelInProgress = false;

    void Update()
    {
        //Debug.Log("Me pase el nivel??? " + levelInProgress);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name== "Death")
        {
            //Va a ser como un engaño. Pensaras que habre borrado con Destroy(gameObject), pero no. Es transparente.
            //Nota, no hago esto para no borrar el canvas, creeme. Decidi poner el canvas en el player para reusarlo en escenas y asi no hacer un copia pega en cada escena.
            this.GetComponent<Renderer>().material = dissapear;
            //Es la flecha el child 0.
            this.transform.GetChild(0).GetComponent<Renderer>().material = dissapear;
            Debug.Log(this.GetComponent<Renderer>().material.name);
        }
        if (levelInProgress)
        {
            StartCoroutine(changeScene());
        }
        //Debug.Log("Colisiono");
        //Le digo al script de move (movimiento del player) que puede saltar.
        move.jump = true;

    }

    

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Door")
        {
            Debug.LogWarning("Success");
            levelInProgress = true;
        }
    }
    IEnumerator changeScene()
    {
        //Estos minisegundos los pongo para que el move pueda desubscribirse del deadEvent (resumen, para prevenir un bug).
        yield return new WaitForSeconds(0.3f);
        //Es un cambio de niveles rapido.
        if(SceneManager.GetActiveScene().name== "Level1")
        {
            SceneManager.LoadScene("Level2");
            levelInProgress = false;
        }
        if (SceneManager.GetActiveScene().name == "Level2")
        {
            SceneManager.LoadScene("Level3");
            levelInProgress = false;
        }
        if (SceneManager.GetActiveScene().name == "Level3")
        {
            SceneManager.LoadScene("Succes");
        }
    }

}
