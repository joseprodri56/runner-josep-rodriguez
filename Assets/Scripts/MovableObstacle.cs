﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableObstacle : MonoBehaviour
{

    //Serializablefield con rango lo que hace es poner en el editor un ajustador (no se como llamarlo) o algo asi
    [SerializeField(), Range(1, 20)]
    int distance;
    //Es un boolean que lo usaremos para indicar a donde movera. False empezara a la derecha, true a la izquierda
    public bool direction= false;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(moving());
    }

    private IEnumerator moving()
    {
        //Entra a la corrutina, la primera vez espera 1 segundo, cambia el booleano y vuelve a entrar a su corrutina y asi continuadamente.
        //Teoricamente no tendria que petar el codigo ya que inicializo la corrutina con un start.
        yield return new WaitForSeconds(1f);
        direction = !direction;
        StartCoroutine(moving());
    }




    // Update is called once per frame
    void Update()
    {
        //Debug.Log(this.GetComponent<Rigidbody>().velocity.x);
        //Cada segundo el boolean cambia, y se movera a la izquierda, luego a la derecha, y asi continuadamente.
        if (direction) this.GetComponent<Rigidbody>().velocity = new Vector3(-distance, this.GetComponent<Rigidbody>().velocity.y, this.GetComponent<Rigidbody>().velocity.z);
        else this.GetComponent<Rigidbody>().velocity = new Vector3(distance, this.GetComponent<Rigidbody>().velocity.y, this.GetComponent<Rigidbody>().velocity.z);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "player")
        {
            //Si colisiona, el jugador salta.
            collision.rigidbody.velocity = transform.up * 5f;
            //Nota: los collisions no requieren mucho el uso del getcomponent (que yo sepa). Con solo decirle collision.transform o collision.rigidbody ya es suficiente
            collision.transform.transform.eulerAngles = new Vector3(0, collision.transform.transform.eulerAngles.y, collision.transform.transform.eulerAngles.z);
            StartCoroutine(freezeRotationforSeconds(collision.rigidbody));
        }
    }

    IEnumerator freezeRotationforSeconds(Rigidbody playerRB)
    {
        //playerRB.constraints = RigidbodyConstraints.FreezeRotationY;
        playerRB.constraints = RigidbodyConstraints.FreezeRotation;
        //playerRB.constraints = RigidbodyConstraints.FreezeRotationZ;
        yield return new WaitForSeconds(0.5f);
        //Esto desactivara el freeze rotation.
        playerRB.constraints &= ~RigidbodyConstraints.FreezeRotation;

    }
}
