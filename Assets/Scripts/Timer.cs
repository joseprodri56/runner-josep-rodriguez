﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    GameObject sun;
    public static int seconds;
    GameObject timeGameOBject;
    Text timeTextValue;
    //Este boolean parara la corrutina
    bool stop= false;
    private float timeD;
    //Siglas: Sun Game Object
    //private GameObject sunGO;
    // Start is called before the first frame update
    void Start()
    {
        timeGameOBject = GameObject.Find("TimeText");
        timeTextValue = timeGameOBject.GetComponent<Text>();
        timeTextValue.text = "FreeMode";

        if (SceneManager.GetActiveScene().name != "Debugger")
        {
            //Solo un gameobject tendra el tag sun.
            //Puedo hacerlo por nombre pero por si acaso no lo hago
            sun = GameObject.FindGameObjectWithTag("sun");
            //Pensabas que no funcionarian las escenas sin entrar al menu??? Pues no, he prevenido esto.
            if (DifficultyMaker.difficulty == null)
            {
                timeD = 1f;
            }
            else if (DifficultyMaker.difficulty.time > 0)
            {
                timeD = DifficultyMaker.difficulty.time;
            }
            //Supondremos que hay un bug y ha puesto 0 segundos o menos
            else
            {
                timeD = 1f;
            }
            //Usare tag mas por comodidad. El nombre sera Directional Light.
            //sunGO = GameObject.FindWithTag("sun");
            
            seconds = 60;

            StartCoroutine(oneday());
        }
        GameManager.isDeadevent += GameManager_isDeadevent;
    }

    private void GameManager_isDeadevent(string cause)
    {
        if(cause== "Falling")
        {
            stop = true;
        }
        //Una vez recorre esto, para que seguir estando subscrito al evento.
        GameManager.isDeadevent -= GameManager_isDeadevent;
    }

    private IEnumerator oneday()
    {
        //Debug.Log(seconds);
        //Limite: 60 segundos. El parametro seconds es quien controlara el sol.
        //Y= -30
        //Hay un bug raro en la rotacion en el que se me mueve tambien la "x" y la "z"
        
        timeTextValue.text = "Time: " + seconds;
        if (seconds > 0 && !stop)
        {
            sun.transform.Rotate(0, 3f, 0);
            seconds--;
            //Puedes editar el tiempo y cambiarlo todas las veces que quieras, que no ejecutara ningun error.
            yield return new WaitForSeconds(timeD);
            StartCoroutine(oneday());

        }
        
    }

    // Update is called once per frame
    void Update()
    {
       
        //this.transform.Rotate(this.transform.rotation.x, 0.5f, this.transform.rotation.z);
    }
}
