﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    
    public delegate void DeadDelegate(String cause);
    public static event DeadDelegate isDeadevent;
    //public delegate void Gravityeffect(bool b);
    //public static event Gravityeffect UnHook;

    GameObject player;
    //Texto del tiempo
    GameObject timeGameOBject;
    Text timeTextValue;
    //Texto de GameOver
    GameObject GameOverGO;
    Text GameOverText;


    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("You start in level " + DifficultyMaker.difficulty.name);
        player = GameObject.Find("player");
        //Nota: Tambien se puede cambiar el texto con un gameobjectNombre.GetComponent<Text>().text.
        timeGameOBject = GameObject.Find("TimeText");
        timeTextValue = timeGameOBject.GetComponent<Text>();
        GameOverGO = GameObject.Find("GameOverReason");
        GameOverText = GameOverGO.GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {
        //Lo hago corrutina debido a un bug del personaje cuando se muere
        StartCoroutine(levelControls());
        if (Input.GetKeyDown("space") || Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            //StartCoroutine(switchingGrav());

        }
        if(timeTextValue.text== "Time: 0")
        {
            //Muerto por el toque de queda
            GameOverText.text = "Game Over The 'Guardia Civil' came to you and arrested you";
            timeTextValue.fontSize = 20;
            timeTextValue.text = "Press 'R' or 'RB' to continue";
            //Hago el invoke en caso de que no haiga nadie subscribido al evento (porque habran casos)
            isDeadevent?.Invoke("TimeOut");
            
        }

        //Debug.Log(player.GetComponent<Renderer>().material.name);
        //Ojo, el contains no es lo mismo que usar el == (aparte que es obvio).
        if (player.GetComponent<Renderer>().material.name.Contains("Transparent (Instance)"))
        {
            //Muerto por caida (imaginate que el suelo, en vez de ser lava, es coronavirus).
            isDeadevent?.Invoke("Falling");
            timeTextValue.fontSize = 20;
            timeTextValue.text = "Press 'R' or 'RB' to continue";
            GameOverText.text = "Game Over You have coronavirus cause the floor is COVID";


        }
    }

    IEnumerator levelControls()
    {
        //Boton RB
        if (Input.GetKeyDown("r") || Input.GetKeyDown(KeyCode.Joystick1Button5))
        {
            yield return new WaitForSeconds(0.2f);
            Scene thisScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(thisScene.name);
        }
    }

    //IEnumerator switchingGrav()
    //{
      //  UnHook?.Invoke(true);
      //  yield return new WaitForSeconds(0.5f);
      //  UnHook?.Invoke(false);
    //}
}
