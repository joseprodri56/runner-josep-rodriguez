﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        this.transform.Rotate(20, 0, 0);
        player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        //this.transform.Rotate(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0);
        this.transform.position = new Vector3(player.GetComponent<Transform>().position.x, player.GetComponent<Transform>().position.y + 1.5f, player.GetComponent<Transform>().position.z - 4);
        //this.transform.Rotate(Input.GetAxis("RightStick Horizontal"), Input.GetAxis("RightStick Vertical"), 0);
        //Debug.Log(Input.GetAxis("RightStick Horizontal") + " " + Input.GetAxis("RightStick Vertical"));

            

    }
}
