﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuOnclick : MonoBehaviour
{
    //Nota, el script esta en el eventSystem del menu.
    public void goToDebugging()
    {
        //Entras al debugging.
        SceneManager.LoadScene("Debugger");
    }

    public void levelLoading(DifficultySO d)
    {
        //Asignara una clase la dificultad
        DifficultyMaker.difficulty= d;
        //Y cargara el nivel 1.
        SceneManager.LoadScene("Level1");
    }


    public void lanternMoveEnabled()
    {
        DifficultyMaker.lanternMove = !DifficultyMaker.lanternMove;
        Debug.Log(DifficultyMaker.lanternMove);
    }
}
