﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class DifficultySO : ScriptableObject
{
    
    public string name;
    //El time seran el tiempo que pasara 1 segundo.
    //El juego teoricamente tiene 60 segundos, si le pones al time un 1, seran 60 segundos.
    //Si pones al time un 0.5, pasaran 30 segundos porque el timer contara 1 segundo como 0.5 segundos.
    
    public float time;
}
